
################################################################################
#
#   Torus implements fractional part of real numbers.
#
class Torus < Numeric

    class ScalarForTorus < Numeric   # :nodoc:
        def initialize(value)
            @value = value
        end

        def + torus
            case @value
            when Torus
                raise "Implementation error! Please contact developers."
            when Integer, Float
                return torus.clone if @value == 0
                raise "Addition of #{@value.class} and #{Torus} undefined."
            else
                raise "Addition of #{@value.class} and #{Torus} undefined."
            end
        end

        def - torus
            @value + (-torus)
        end

        def * torus
            case @value
            when Integer
                torus.scalar_mult(@value)
            else
                raise "Multiplication of #{@value.class} and #{Torus} undefined."
            end
        end

        def / torus
            raise "Division by #{Torus} undefined."
        end

        def == torus
            #TODO consider `torus == @value` ??
            raise "Comparison of #{@value.class} and #{Torus} undefined."
        end
    end
    private_constant :ScalarForTorus   #?# needed?

    def - other
        self + (-other)
    end

    #   Undefined for Torus.
    def * other
        raise "Multiplication undefined for #{self.class}."
    end

    #   Undefined for Torus.
    def / other
        raise "Division undefined for #{self.class}."
    end

    #   N.b.: > is only used for a comparison with 0, this is when a PolyN is printed (which sign to choose).
    def > other
        other.to_i == 0
    end

    #   N.b.: < is only used for a comparison with 0, this is when a PolyN is printed (which sign to choose).
    def < other
        not self > other
    end

    def coerce(other)
        [ScalarForTorus.new(other), self]
    end

    def const_term
        self
    end

    def to_c
        to_f.to_c
    end

    def to_torus
        self
    end

    def to_s   # :nodoc:
        raise "#{self.class}##{__method__} unimplemented!"
    end
    alias_method :inspect, :to_s
end
