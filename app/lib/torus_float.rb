require 'torus'

class TorusInt < Torus
end

class TorusFloat < Torus   # :nodoc:

    private def initialize(val, prec)
        raise "#{self.class} initialization error: precision cannot be 0." if prec.to_i == 0

        case val
        when Float
            @val = val - val.floor
        else
            @val = val.to_i.to_f / (1 << prec.to_i)
            @val = @val - @val.floor
        end
    end

    def self.[](val, prec)
        TorusFloat.new(val, prec)
    end

    def self.zero
        TorusFloat[0.0, nil]
    end

    def zero
        self.class[0.0, nil]
    end

    def clone
        self.class[@val]
    end

    def to_s
        @val.to_s
    end
    alias_method :inspect, :to_s

    def to_f
        @val
    end

    def to_ti
        TorusInt[self.to_f]   #?#
    end

    def scalar_mult(scalar)
        self.class[@val * scalar.to_i]
    end

    def + other
        case other
        when Torus
            self.class[@val + other.to_f]
        when Integer, Float
            return clone if other == 0
            raise "Addition of #{Torus} and #{other.class} undefined."
        else
            other.coerce(self).reduce(__method__)
        end
    end

    def -@
        self.class[-@val]
    end

    def == other
        raise "Comparison of #{self.class} and #{other.class} undefined." unless other.is_a? self.class
        @val == other.to_f
    end

    def get_bit_at_minus(j)   #TODO
        raise "#{self.class}:#{__method__} not implemented yet."
    end

    def decompose(bg_bits, l)   #TODO
        m = ((1 << l) * @val).round

        d = Vector.zero(l)

        l.times do |p|
            d[l-1-p] = m % 2
            m /= 2
        end

        d
    end

    #!# not checked at all
    def round_numer_at(newprec)
        ((1 << newprec.to_i) * @val).round
    end

    def round_at(newprec)
        self.class[round_numer_at(newprec), newprec]
    end

end
