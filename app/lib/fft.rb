require 'matrix'
require 'cplx_int'

class Complex
    #   Round complex number at given decimal precision
    def round(*round_args)
        Complex(*[self.real.round(*round_args), self.imag.round(*round_args)])
    end
end



################################################################################
#
#   +FourierImage+ holds coefficients of Fourier Transform
#
class FourierImage < Array

    def initialize(*args)   # :nodoc:
        super
    end

    #   Point-wise multiplication
    def point_wise(other)
        raise TypeError, "Incompatible class: #{other.class}" unless other.is_a? self.class
        raise "Dimension mismatch" unless self.size == other.size

        self.class[*self.zip(other).map{|t|t.first * t.last}]
    end


    # ==========================================================================
    #
    #   :section: Direct and Inverse transformations
    #

    #   Returns naive DFT of self as +FourierImage+
    def dft_naive(inv = false)
        PolyN.coeffs(self).dft_naive(inv)
    end

    #   Returns inverse naive DFT of self as +FourierImage+
    def idft_naive
        dft_naive true
    end

    #   Returns plain FFT of self as +FourierImage+
    def fft_plain(inv = false)
        PolyN.coeffs(self).fft_plain(inv)
    end

    #   Returns inverse plain FFT of self as +FourierImage+
    def ifft_plain
        fft_plain true
    end

    #   Returns negacyclic FFT of self as +FourierImage+
    def fnt(inv = false)
        PolyN.coeffs(self).fnt(inv)
    end

    #   Returns inverse negacyclic FFT of self as +FourierImage+
    def ifnt
        fnt true
    end

    #   Returns alternative negacyclic FFT of self as +FourierImage+
    def afnt(inv = false)
        PolyN.coeffs(self).afnt(inv)
    end

    #   Returns alternative inverse negacyclic FFT of self as +FourierImage+
    def iafnt
        afnt true
    end

    def to_s   # :nodoc:
        "#{self.class}\n\t" + self.map{|e|e.round(3)}.to_s
    end
    alias_method :inspect, :to_s

end



################################################################################
#
#   Module that extends +PolyN+ by Fast Fourier Transform methods
#
module FFT

    #
    #   Returns Discrete Fourier Transform of self as +FourierImage+ (naive).
    #
    def dft_naive(inv = false)
        f = Vector.zero self.deg

        self.deg.times do |l|
            # calculate l-th Fourier basis vector
            # bl_j = exp(-2 pi i j l / N) = cos(<arg>) + i sin(<arg>)
            #            +2 in case of inverse transform
            blRe = Vector.elements(Array.new(self.deg){|j| Math::cos(2 * Math::PI * j * l / self.deg) }, false)
            blIm = Vector.elements(Array.new(self.deg){|j| Math::sin((inv ? 2 : -2) * Math::PI * j * l / self.deg) }, false)

            # dot product (i.e., sum)
            f[l] = Vector.elements(self.coeffs).dot(blRe) + 1i * Vector.elements(self.coeffs).dot(blIm)
        end

        # for inverse transform, divide by its size
        f /= self.deg if inv

        FourierImage[*f]
    end

    #
    #   Returns plain Fast Fourier Transform of itself as +FourierImage+
    #
    def fft_plain(inv = false)
        FourierImage[*DFTable.new(@coeffs.map{|c|c.to_c}).fft_plain(inv)]   #TODO: c.to_ci
    end

    #
    #   Returns negacyclic FFT of itself as +FourierImage+
    #
    #   This implementation skips zeros.
    #
    def fnt(inv = false)
        FourierImage[*DFTable.new(@coeffs.map{|c|c.to_c}).fnt(inv)]   #TODO: c.to_ci
    end

    #
    #   Returns negacyclic FFT of itself as +FourierImage+ (only first half)
    #
    #   This implementation skips zeros as well as conjugates.
    #
    def afnt(inv = false)
        FourierImage[*DFTable.new(@coeffs.map{|c|c.to_c}).afnt(inv)]   #TODO: c.to_ci
    end



    ############################################################################
    #
    #   Class that implements most FFT functionality
    #
    class DFTable

        #   DBG
        attr_accessor :t

        #   Initialize
        def initialize(a)
            a = a.to_a

            raise "#{DFTable} can only be initialized with an even non-zero number of values." unless a.size % 2 == 0 && a.size > 0

            @t = Array.new(a.size / 2) do |r|
                Array.new(2) do |c|
                    a[2*r + c]
                end
            end
        end

        #   Returns total number of elements
        def size
            2 * @t.size
        end


        # ======================================================================
        #
        #   :section: I/O Operations
        #

        #   Fill by columns
        def fill_by_cols(col0, col1)
            col0 = col0.to_a
            col1 = col1.to_a

            raise "#{DFTable} can only be filled with equal-sized columns." unless col0.size == col1.size

            @t = Array.new(col0.size) do |r|
                Array.new(2) do |c|
                    (c == 0) ? col0[r] : col1[r]
                end
            end
        end

        #   Output single array by columns
        def out_by_cols
            @t.transpose.flatten
        end

        #   Output single array by U-shape
        def out_by_U
            #   after afnt2_fin, only 1-element array is in @t: [[x]]
            if @t[0].size == 1
                @t[0]
            else
                @t.transpose[0] + @t.transpose[1].reverse
            end
        end


        # ======================================================================
        #
        #   :section: End of Recursion
        #

        #   Fourier transform of last 2-element row
        def ft2_plain_fin
            raise "Final point of recursion reached by a multi-row table!" unless @t.size == 1
            @t[0] = [@t.first[0] + @t.first[1], @t.first[0] - @t.first[1]]
        end

        #   Negacyclic Fourier transform of last 2-element row
        def fnt2_fin
            raise "Final point of recursion reached by a multi-row table!" unless @t.size == 1
            @t[0] = [@t.first[0] - 1i * @t.first[1], @t.first[0] + 1i * @t.first[1]]   #!# this will work after coertion is implemented for CplxInt
        end

        #   Alternative Negacyclic Fourier transform of last 2-element row
        #   => at this point, total number of elements is halved
        def afnt2_fin
            raise "Final point of recursion reached by a multi-row table!" unless @t.size == 1
            @t[0] = [@t.first[0] - 1i * @t.first[1]]   #!# this will work after coertion is implemented for CplxInt
        end


        # ======================================================================
        #
        #   :section: Multiplication by +omega+
        #

        #   Calculation of +omega+'s
        def omega(j,n, inv = false)
            j = -j if inv

            (Math::cos(2 * Math::PI * j / n) - 1i * Math::sin(2 * Math::PI * j / n))
            #TODO:
            # CplxInt.omega_N(j, n, @bnd?, @prec?)

            #~ inv ?
                #~ (Math::cos(2 * Math::PI * j / n) + 1i * Math::sin(2 * Math::PI * j / n)) :
                #~ (Math::cos(2 * Math::PI * j / n) - 1i * Math::sin(2 * Math::PI * j / n))
        end

        #   Multiply by omega_N
        def mult_omega_plain(inv = false)
            @t.map!.with_index do |row, ri|
                [
                    row[0] * 1, \
                    row[1] * omega(ri, self.size, inv)   #!# this will work after coertion is implemented for CplxInt
                ]
            end
        end

        #   Multiply by negacyclic omega_N
        def mult_omega_fnt
            @t.map!.with_index do |row, ri|
                [
                    row[0] * 1, \
                    row[1] * omega(2 * ri + 1, 2 * self.size)   #!# this will work after coertion is implemented for CplxInt
                ]
            end
        end

        #   Multiply by accelerated negacyclic omega_N
        def mult_omega_afnt
            @t.map!.with_index do |row, ri|
                [
                    row[0] * 1, \
                    row[1] * omega(2 * ri + 1, 4 * self.size)   #!# this will work after coertion is implemented for CplxInt
                ]
            end
        end


        # ======================================================================
        #
        #   :section: Row-wise Transform
        #

        #   Fourier transform of each 2-element row
        def ft2_plain_rows
            @t.map! do |row|
                [row[0] + row[1], row[0] - row[1]]
            end
        end

        #   Negacyclic Fourier transform of each 2-element row (equal to the plain one)
        def fnt2_rows
            ft2_plain_rows
        end

        #   Accelerated Negacyclic Fourier transform of each 2-element row
        def afnt2_rows
            @t.map! do |row|
                [row[0] + row[1], (row[0] - row[1]).conj]
            end
        end


        # ======================================================================
        #
        #   :section: Transforms of Large Input Arrays
        #

        #   Fast Fourier Transform (plain)
        def fft_plain(inv = false, top_level = true)
            if @t.size == 1
                ft2_plain_fin
            else
                #   FFT each column by splitting into halves (prepare new instance, reorder results)
                col0, col1 = @t.transpose
                col0 = DFTable.new(col0).fft_plain(inv, false)
                col1 = DFTable.new(col1).fft_plain(inv, false)
                fill_by_cols(col0, col1)

                #   multiply by omega_N & FT2 rows
                mult_omega_plain(inv)
                ft2_plain_rows
            end

            #   read output by columns
            (inv and top_level) ?
                self.out_by_cols.map{|e| e / self.size } :
                self.out_by_cols
        end

        #   Fast Negacyclic Fourier Transform
        def fnt(inv = false)
            #   FNFT^-1 performs a plain FFT^-1 followed by multiplication by +omega+'s
            if inv
                return self.fft_plain(true).map.with_index{|res,idx| res * omega(idx, 2 * self.size, inv)}
            end

            if @t.size == 1
                #   corresponds to FFT_4 with zeros in the second half
                fnt2_fin
            else
                #   FFT each column by splitting into halves (prepare new instance, reorder results)
                col0, col1 = @t.transpose
                col0 = DFTable.new(col0).fnt
                col1 = DFTable.new(col1).fnt
                fill_by_cols(col0, col1)

                #   multiply by negacyclic omega_N & FT2 rows
                mult_omega_fnt
                fnt2_rows
            end

            #   read output by columns
            self.out_by_cols
        end

        #   Yet twice faster FNFT
        #   Employs the fact, that the input is real-valued, then the coefficients are Hermitian (i.e., conjugate-symmetric)
        def afnt(inv = false)
            if inv
                #TODO this does not enjoy the N/2 advantage
                #   actually it seems that it will never do
                return DFTable.new(@t.flatten + @t.flatten.reverse.map(&:conj)).fnt(inv)
            end

            if @t.size == 1
                #   last point of recursion
                afnt2_fin
            else
                #   FFT each column by splitting into halves (prepare new instance, reorder results)
                col0, col1 = @t.transpose
                col0 = DFTable.new(col0).afnt
                col1 = DFTable.new(col1).afnt
                #   now, it has half that size
                fill_by_cols(col0, col1)

                #   multiply by alternative negacyclic omega_N & FT2 rows
                mult_omega_afnt
                afnt2_rows
            end

            #   read output by columns
            self.out_by_U
        end

        #   Printable format
        def to_s
            "#{self.class}\n" + @t.map{|row| "| " + row.map{|e| e.round(3).to_s }.join(", ") }.join("\n")
        end
        alias_method :inspect, :to_s
    end
    private_constant :DFTable

end


#   Ideas & Notes

#   Devise a transform with sines and cosines, on Reals only.
#   Point-wise multiplication can be left as: ( a*c - b*d , a*d + b*c ).
#
#   => gets fucking creepy ...
