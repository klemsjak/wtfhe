require 'matrix'
require 'poly_N'
require 'sample'



################################################################################
#
#   Implementation of TRLWE sample.
#
class TrlweSample < Sample

    def initialize(vector, noise)   # :nodoc:
        super
    end

    #TODO: consider copy param
    def TrlweSample.elements(vectary, noise)
        TrlweSample.new(Vector.elements(vectary), noise)
    end

    def TrlweSample.triv_zero(dim, tau)
        TrlweSample.new(Vector.zero(dim + 1), Noise.zero(tau))
    end

    def to_trlwe_sample
        self
    end

    #   Returns degree of TRLWE sample.
    def deg
        @sample.first.deg
    end

    #   Returns dimension of TRLWE sample.
    def dim
        @sample.size - 1
    end

    #   Returns mask +a+ as a Vector
    def a
        @sample.slice(0..-2)
    end

    #   Returns masked plaintext +b+.
    def b
        @sample[-1]
    end

    #
    #   Implements TRLWE Gadget Decomposition Algorithm.
    #
    #   Returns a row vector (as a single-row Matrix) of PolyN's with binary coeffs.
    #
    #   +gamma+     B_g = 2^+gamma+, i.e., +gamma+-bit numbers are returned,
    #   +l+         highest power of B_g, till which decomposition is performed.
    #
    def decompose(gamma, l)
        e = Vector.zero(0)

        # @sample is a Vector of PolyN's over Torus
        @sample.each do |ai|
            v = Vector.zero(l)
            # ai is a PolyN over Torus
            ai.each_with_power do |aij, j|
                # aij is a Torus coefficient
                v += aij.to_torus.decompose(gamma, l).map do |c|
                    # aij decomposed is a Vector of +gamma+-bit numbers, just need to be multiplied by X^j
                    PolyN.const(c, deg).mul_x(j)
                end
            end
            e = e.push(*v)
        end

        Matrix.row_vector(e)
    end
end
