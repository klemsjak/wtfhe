require 'torus'

class TorusFloat < Torus
end



################################################################################
#
#   TorusInt implements Torus as a fraction of the form +numerator+/2^+precision+.
#
class TorusInt < Torus

    #   Numerator of the fraction.
    attr_reader :numer
    #   Precision of the fraction, i.e., denominator = 2^+PREC+.
    attr_reader :PREC

    private def initialize(numer, prec)
        @PREC = prec.to_i
        raise "#{self.class} initialization error: precision cannot be <= 0." if @PREC <= 0

        case numer
        when Integer
            @numer = numer & ((1 << @PREC) - 1)   # ... == numer % 2^@PREC-1
        when Float
            @numer = (numer * (1 << @PREC)).round & ((1 << @PREC) - 1)   # (val * 1000000) & 111111
        else
            raise "#{self.class} initialization error: #{numer.class} class is not supported."
        end
    end

    #
    #   Initializes a TorusInt with a numerator and a precision.
    #
    #   +numer+:: fraction numerator,
    #   +prec+::  fraction bit-precision, i.e., denominator = 2^+prec+.
    #
    def self.[](numer, prec)
        TorusInt.new(numer, prec)
    end

    #   Initializes a TorusInt equal to zero with precision +prec+.
    def self.zero(prec)
        TorusInt.new(0, prec)
    end

    #   Returns a new TorusInt equal to zero with the same precision.
    def zero
        self.class.zero(@PREC)
    end

    def clone
        self.class[@numer, @PREC]
    end

    #   Precision of the fraction, i.e., denominator = 2^+PREC+.
    def prec
        @PREC
    end

    def to_s
        @numer.to_s + "/2^" + @PREC.to_s
    end
    alias_method :inspect, :to_s

    def to_f
        @numer.to_f / (1 << @PREC)
    end

    #   Converts TorusInt to TorusFloat.
    def to_tf
        TorusFloat[self.to_f]
    end

    def scalar_mult(scalar)
        self.class.new(@numer * scalar.to_i, @PREC)
    end

    def + other
        case other
        when Torus
            if other.is_a?(TorusInt) and other.prec == @PREC
                self.class.new(@numer + other.numer, @PREC)
            else
                sum_numer, sum_prec = @PREC > other.prec ? [@numer + (other.numer << @PREC - other.prec), @PREC] : [(@numer << other.prec - @PREC) + other.numer, other.prec]
                self.class.new(sum_numer, sum_prec)
            end
        when Integer, Float
            return clone if other == 0
            raise "Addition of #{Torus} and #{other.class} undefined."
        else
            other.coerce(self).reduce(__method__)
        end
    end

    def -@
        self.class.new(-@numer, @PREC)
    end

    def == other
        case other
        when self.class
            self.numer << other.prec == other.numer << self.prec
        when Numeric
            other.coerce(self).reduce(__method__)
        else
            raise "Comparison of #{self.class} and #{other.class} undefined." unless other.is_a? self.class
        end
    end

    #   Returns a numerator rounded at precision +newprec+, i.e., +round+(+frac+ * 2^+newprec+).
    def round_numer_at(newprec)
        (@numer >> (@PREC - newprec)) + ((@numer >> (@PREC - newprec - 1)) & 1)
    end

    #   Returns a TorusInt rounded at precision +newprec+.
    def round_at(newprec)
        self.class[round_numer_at(newprec), newprec]
    end

    #   Returns a bit of numerator rounded at +newprec+ (cf. #round_numer_at) at position +j+.
    def get_bit_at_minus(j, newprec = @PREC)   # from 2^-1 till 2^-prec
        j = j.to_i
        raise "#{self.class}:#{__method__} invalid argument." if j <= 0 or j > @PREC or j > newprec
        (round_numer_at(newprec) >> (newprec - j)) & 1
    end

    #   Returns the numerator (rounded and) decomposed into a Vector of +l+ +bg_bits+-bit numbers.
    #   E.g., 11011 10011 101 -> (11011, 10111) for +bg_bits+ = 5 and +l+ = 2.
    def decompose(bg_bits, l)
        r = round_numer_at(bg_bits * l)
        d = Vector.zero(l)

        l.times do |p|
            d[l-1-p] = (r >> (bg_bits * p)) & ((1 << bg_bits) - 1)   # shift to the right and extract +bg_bits+
        end

        d
    end

end
