require 'trlwe'



################################################################################
#
#   Implementation of TLWE, which is TRLWE with degree = 1.
#
class TLWE < TRLWE

    def initialize(tau, dim, alpha = 0, k = nil)
        super(tau, 1, dim, alpha, k)
    end

end
