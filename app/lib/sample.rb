require 'matrix'
require 'noise'

class Matrix

    #
    #   Prints Matrix in a readable format.
    #
    #       Matrix
    #       | 45/64 + 42/64 X + 34/64 X^2 + 39/64 X^3, 28/64 + 6/64 X + 62/64 X^2 + 49/64 X^3
    #       | 48/64 + 17/64 X + 10/64 X^2 + 8/64 X^3, 59/64 + 16/64 X + 5/64 X^2 + 12/64 X^3
    #       | 45/64 + 27/64 X + 11/64 X^2 + 43/64 X^3, 27/64 + 47/64 X + 2/64 X^2 + 16/64 X^3
    #
    def to_s
        "#{self.class}\n" + @rows.map{|row| "| " + row.map{|e| e.to_s }.join(", ") }.join("\n")
    end
    alias_method :inspect, :to_s

    def to_matrix   # :nodoc:
        self
    end

end

class Vector   # :nodoc:
    def push(*val)
        self.class.elements(@elements.dup.push(*val), false)
    end

    def slice(rangindex)
        self.class.elements(@elements.slice(rangindex))
    end
end



################################################################################
#
#   Implementation a generic sample.
#
class Sample

    class ScalarForSample < Numeric   # :nodoc:
        def initialize(value)
            @value = value
        end

        def * sample
            case @value
            when Numeric
                sample.scalar_mult(@value)
            else
                raise "Multiplication of #{@value.class} and #{sample.class} undefined."
            end
        end
    end
    private_constant :ScalarForSample   #?# needed?

    #   Sample (Vector or Matrix)
    attr_reader :sample
    #   Actual amount of noise (class Noise)
    attr_reader :noise

    def initialize(sample, noise)
        @sample = sample    # checked by child constructors
        @noise = noise.to_noise
    end

    def update_noise(noise)
        @noise = noise.to_noise
    end

    def coerce(other)
        [ScalarForSample.new(other), self]
    end

    def == other
        raise "Incompatible operation :#{__method__} for #{self.class} and #{other.class}" unless self.class == other.class
        @sample == other.sample
    end

    def + other
        raise "Incompatible operation :#{__method__} for #{self.class} and #{other.class}" unless self.class == other.class
        self.class.new(@sample + other.sample, @noise.add(other.noise))
    end

    def -@
        self.class.new(-@sample, @noise.clone)
    end

    def - other
        self + (-other)
    end

    def scalar_mult(scalar)
        # the order of operands is important here!
        # n.b.: scalar can be PolyN
        self.class.new(scalar * @sample, @noise.scalar_mult(scalar))
    end

    def to_s   # :nodoc:
        "#{self.class}: #{@sample}\n" + @noise.to_s
    end
    alias_method :inspect, :to_s

end
