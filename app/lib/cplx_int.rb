require 'float_int'

class Integer   # :nodoc:
    def to_ci(bnd, prec)
        CplxInt[self.to_fi(bnd, prec), FloatInt.zero(bnd, prec)]
    end
end

class Float   # :nodoc:
    def to_ci(bnd, prec)
        CplxInt[self.to_fi(bnd, prec), FloatInt.zero(bnd, prec)]
    end
end

class Torus   # :nodoc:
    def to_ci(bnd, prec)
        raise "Conversion of #{self.class} to #{CplxInt} with different parameters." unless prec == self.prec
        CplxInt[FloatInt.from_torus(self, bnd), FloatInt.zero(bnd, prec)]
    end
end

class Complex   # :nodoc:
    def to_ci(bnd, prec)
        CplxInt.from_cplx(self, bnd, prec)
    end
end



################################################################################
#
#   CplxInt implements complex numbers using +FloatInt+.
#
class CplxInt   #TODO < Numeric ?

    class ScalarForCplxInt #TODO < Numeric   # :nodoc:
        def initialize(value)
            @value = value
        end

        def + ci
            ci + @value
        end

        def - ci
            (-ci) + @value
        end

        def * ci
            ci * @value
        end

        def / ci
            raise "Division by #{CplxInt} not implemented yet."
        end

        def == ci
            ci == @value
        end
    end
    private_constant :ScalarForCplxInt   #?# needed?

    #   Real part.
    attr_reader :re
    #   Imaginary part.
    attr_reader :im
    #   Bound of underlying +FloatInt+.
    attr_reader :BND
    #   Precision of underlying +FloatInt+.
    attr_reader :PREC

    private def initialize(re, im)
        @re = re.to_fi
        @im = im.to_fi
        raise "#{self.class} initialization error: incompatible parts with #{@re.bnd} and #{@im.bnd} bits of bound, and #{@re.prec} and #{@im.prec} bits of precision, respectively." unless @re.bnd == @im.bnd and @re.prec == @im.prec
        @BND = @re.bnd
        @PREC = @re.prec
    end

    #
    #   Initializes a CplxInt with real and imaginary part.
    #
    #   +re+::    real part,
    #   +im+::    imaginary part.
    #
    def self.[](re, im)
        CplxInt.new(re, im)
    end

    #
    #   Initializes +omega_N^j+ for FFT.
    #
    #--
    #   +j+::     power of +omega_N^j+,
    #   +n+::     parameter +N+.
    #
    def self.omega_N(j, n, bnd, prec)
        raise "Error in #{omega_N}: N must be a positive integer." if n.to_i <= 0
        arg = -2.0 * Math::PI * j.to_i / n.to_i
        CplxInt.new(FloatInt[Math.cos(arg), bnd, prec], FloatInt[Math.sin(arg), bnd, prec])
    end

    #   Initializes a CplxInt equal to zero with bound +bnd+
    #   and precision +prec+.
    def self.zero(bnd, prec)
        CplxInt.new(FloatInt.zero(bnd, prec), FloatInt.zero(bnd, prec))
    end

    #   Returns a new CplxInt equal to zero with the same precision and bound.
    def zero
        self.class.zero(@BND, @PREC)
    end

    #   Returns TODO.
    def self.from_cplx(cplx, bnd, prec)
        CplxInt.new(FloatInt.from_flt(cplx.real, bnd, prec), FloatInt.from_flt(cplx.imag, bnd, prec))
    end

    #   Returns TODO.
    def from_cplx(cplx)
        self.class.from_cplx(cplx, @BND, @PREC)
    end

    def clone
        self.class.new(@re, @im)
    end

    #   Returns self and fails when called on an incompatible object.
    def to_ci(bnd, prec)
        raise "Conversion of self to #{CplxInt} with different parameters." unless bnd == @BND and prec == @PREC
        self
    end

    #   Bound of its absolute value.
    def bnd
        @BND
    end

    #   Precision of the fractional part of its Re/Im parts.
    def prec
        @PREC
    end

    def to_s(base = 2)
        "#{self.class} |" + @re.to_s(base) + "| + i |" + @im.to_s(base) + "|"
    end
    alias_method :inspect, :to_s

    def to_c
        @re.to_f + 1i * @im.to_f
    end

    def * other
        case other
        when Integer, Float, Torus, FloatInt
            self.class.new([@re, other].reduce(__method__), [@im, other].reduce(__method__))
        when Complex
            [self, self.from_cplx(other)].reduce(__method__)
        when CplxInt
            self.class.new(@re * other.re - @im * other.im, @re * other.im + @im * other.re)
        else
            other.coerce(self).reduce(__method__)   #TODO
        end
    end

    def + other
        case other
        when Integer, Float, Torus, FloatInt
            self.class.new([@re, other].reduce(__method__), @im)
        when Complex
            [self, self.from_cplx(other)].reduce(__method__)
        when CplxInt
            self.class.new(@re + other.re, @im + other.im)
        else
            other.coerce(self).reduce(__method__)   #TODO
        end
    end

    def -@
        self.class.new(-@re, -@im)
    end

    def - other
        self + (-other)
    end

    def conj
        self.class.new(@re, -@im)
    end
    alias_method :conjugate, :conj

    def == other
        case other
        when Integer, Float, Torus, FloatInt
            @re == other and @im == FloatInt.zero(@BND, @PREC)
        when CplxInt
            @re == other.re and @im == other.im
        #~ when Numeric
            #~ other.coerce(self).reduce(__method__)   #TODO
        else
            raise "Comparison of #{self.class} and #{other.class} undefined." unless other.is_a? self.class
        end
    end

    def coerce(other)
        [ScalarForCplxInt.new(other), self]
    end

end

__END__

    #   Returns a numerator rounded at precision +newprec+, i.e., +round+(+frac+ * 2^+newprec+).
    def round_numer_at(newprec)
        (@numer >> (@PREC - newprec)) + ((@numer >> (@PREC - newprec - 1)) & 1)
    end

    #   Returns a TorusInt rounded at precision +newprec+.
    def round_at(newprec)
        self.class[round_numer_at(newprec), newprec]
    end

    #   Returns a bit of numerator rounded at +newprec+ (cf. #round_numer_at) at position +j+.
    def get_bit_at_minus(j, newprec = @PREC)   # from 2^-1 till 2^-prec
        j = j.to_i
        raise "#{self.class}:#{__method__} invalid argument." if j <= 0 or j > @PREC or j > newprec
        (round_numer_at(newprec) >> (newprec - j)) & 1
    end

    #   Returns the numerator (rounded and) decomposed into a Vector of +l+ +bg_bits+-bit numbers.
    #   E.g., 11011 10011 101 -> (11011, 10111) for +bg_bits+ = 5 and +l+ = 2.
    def decompose(bg_bits, l)
        r = round_numer_at(bg_bits * l)
        d = Vector.zero(l)

        l.times do |p|
            d[l-1-p] = (r >> (bg_bits * p)) & ((1 << bg_bits) - 1)   # shift to the right and extract +bg_bits+
        end

        d
    end

end
