require 'torus_int'
require 'poly_N'
require 'trlwe_sample'



################################################################################
#
#   Implementation of TRLWE cipher.
#
class TRLWE

    #
    #   Initializes TRLWE cipher.
    #
    #   +tau+::         underlying Torus precision,
    #   +deg+::         degree of X^N + 1,
    #   +dim+::         dimension,
    #   +alpha+::       noise parameter alpha (+Integer+),
    #   +key+::         private key, an Array/Vector of binary polynomials modulo X^N + 1.
    #
    def initialize(tau, deg, dim, alpha = 0, key = nil)
        @TAU = tau.to_i
        @DEG = deg.to_i
        @DIM = dim.to_i

        @ALPHA = alpha.to_i

        @KEY = key.nil? ? Vector.elements(Array.new(@DIM){PolyN.rand(@DEG, Integer, 2)}, false) : Vector.elements(key)
    end

    #   Underlying Torus precision.
    def tau
        @TAU
    end

    #   Degree of X^N + 1.
    def deg
        @DEG
    end

    #   Dimension.
    def dim
        @DIM
    end

    #   Sample size.
    def sample_size
        @DIM + 1
    end

    #   Noise parameter alpha.
    def alpha
        @ALPHA
    end

    #   Get Noise instance
    def noise_inst
        Noise.by_ampl_i_uniform(@TAU, @ALPHA)
    end

    #   Get zero Noise instance
    def noise_inst_zero
        Noise.zero(@TAU)
    end

    def to_trlwe # :nodoc:
        self
    end

    #
    #   Returns a fresh TRLWE sample of +mu+ with given +a+.
    #
    #   +mu+::          message (PolyN over Torus) to be sampled,
    #   +a+::           random sampling vector,
    #   +is_noise+::    flag whether noise will be added.
    #
    def sample_with_a(mu, a, is_noise = true)
        #TODO check that mu is a PolyN with Torus elements: mu.to_poly_torus, a.to_vector_of_poly_torus

        noise = is_noise ? noise_inst : noise_inst_zero

        TrlweSample.elements(a.push(mu + @KEY.dot(a) + noise.draw_poly(@ALPHA, self.deg)), noise)
    end

    #
    #   Returns a fresh TRLWE sample of +mu+ with random +a+.
    #
    #   +mu+::          message (PolyN over Torus) to be sampled,
    #   +is_noise+::    flag whether noise will be added.
    #
    def sample(mu, is_noise = true)
        # sampling vector a is a Vector of PolyN's with random TorusInt coeffs
        a = Vector.elements Array.new(@DIM){PolyN.rand(@DEG, TorusInt, @TAU)}, false
        sample_with_a(mu, a, is_noise)
    end

    #
    #   Returns a trivial (noiseless) TRLWE sample of +mu+ with +a+ = (0,0,...,0),
    #   i.e., (0, 0, ..., 0, +mu+).
    #   Does not encrypt!
    #
    #   +mu+::          message (PolyN over Torus) to be sampled.
    #
    def triv_sample(mu)
        # a = (0,0,...,0)
        a = Vector.elements Array.new(@DIM){PolyN.zero(@DEG, TorusInt, @TAU)}, false
        sample_with_a(mu, a, false)
    end

    #
    #   Returns a homogeneous TRLWE sample (i.e., +mu+=0) with random +a+.
    #
    #   +is_noise+::    flag whether noise will be added.
    #
    def homog_sample(is_noise = true)
        sample(PolyN.zero(@DEG, TorusInt, @TAU), is_noise)
    end

    #   Returns phase of a TRLWE sample +ab+.
    def phase(ab)
        ab.b - @KEY.dot(ab.a)
    end

    def to_s   # :nodoc:
        "#{self.class.to_s}\n" + \
        "\t[Torus prec] #{@TAU}, [deg] #{@DEG}, [dim] #{@DIM},\n" + \
        "\t[alpha] #{@ALPHA}, [key] #{@KEY}"
    end
    alias_method :inspect, :to_s

end
