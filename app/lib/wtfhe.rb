require 'matrix'
require 'torus_int'
require 'cplx_int'
require 'poly_N'
require 'tlwe'
require 'trlwe'
require 'trlwe_sample'
require 'trgsw'



################################################################################
#
#   WTFHE is the main cipher class that holds all underlying objects.
#
class WTFHE

    #   Underlying TLWE cipher used for actual WTFHE en/decryption.
    attr_reader :TLWE
    #   Underlying TRGSW cipher used for bootstrapping keys.
    attr_reader :TG
    #   An array of bootstrapping keys, i.e., TRGSW samples of bits of key +K+.
    attr_reader :BK
    #   WTFHE key serves as a key for TLWE as well as for TRGSW (we use circular security assumption).
    attr_reader :K

    #
    #   A set of Baby parameters.
    #--
    #                   plaintext precision
    #                   |   Torus precision
    #                   |   |   dimension
    #                   |   |   |   gamma
    #                   |   |   |   |   l
    #                   |   |   |   |   |   alpha
    #                   |   |   |   |   |   |   key
    @@BABY_PARAMS = [   2,  20, 4,  3,  4,  1,  [1,0,1,1]]

    #
    #   Initializes WTFHE cipher with:
    #
    #   +prec+::        desired plaintext space precision (in bits),
    #   +tau+::         underlying TorusInt precision (in bits, incl. noise),
    #   +dim+::         dimension of underlying TLWE cipher (also key size in bits),
    #   +gamma+::       TRLWE decomposition parameter gamma,
    #   +l+::           TRLWE decomposition parameter l,
    #   +alpha+::       noise magnitude,
    #   +key+::         secret key, a binary Array/Vector, or nil (generates random key),
    #
    def initialize(prec, tau, dim, gamma, l, alpha = 0, key = nil)

        # ----------------------------------------------------------------------
        #   Setup params & check conditions
        #
        @PREC  = prec.to_i.abs
        @TAU = tau.to_i.abs
        @DIM = dim.to_i.abs
        raise "Improved BlindRotate requires even dimension." unless @DIM.even?

        @NU = Math.log2(@DIM).ceil + @PREC   # N = 2^nu where nu ~ log2(n) + prec (i.e., N ~ (n+1) * 2^prec)
        @DEG = 1 << @NU

        @GAMMA = gamma.to_i.abs
        @L = l.to_i.abs

        @ALPHA = alpha.to_i.abs

        #TODO add RNG's
        @KEY = key.nil? ? Vector.elements(Array.new(@DIM){Random.rand(2)}, false) : Vector.elements(key)
        raise "#{self} initialization: wrong key size: #{key.size}." if key.size != @DIM

        #TODO add other param warnings
        # check Torus precision vs. desired precision (very basic check)
        warn "(w) #{self} initialization: There is no space for noise!" if @PREC == @TAU
        raise "#{self} initialization: precision cannot be more than underlying #{TorusInt} precision." if @PREC > @TAU
        # check that 2N <= 2^tau
        raise "#{self} initialization: underlying #{TorusInt} precision is too small to hold N." if @NU > @TAU
        # check gadget matrix resolution
        raise "#{self} initialization: underlying #{TorusInt} precision is too small to hold N." if @GAMMA * @L > @TAU

        # ----------------------------------------------------------------------
        #   Init the main TLWE encryption with key @KEY
        #
        @TLWE = TLWE.new(@TAU, @DIM, @ALPHA, @KEY)

        # ----------------------------------------------------------------------
        #   Generate bootstrapping keys for improved BlindRotate
        #

        # setup the TRGSW key:
        #       (0, 0, ...,                  0,    KEY)           so that TLWE key = last @DIM coeffs of TRGSW key
        tgkey = [PolyN.coeffs([0]*(@DEG - @DIM) + @KEY.to_a)]   # n.b., scenario without KeySwitch

        @TG = TRGSW.new(@TAU, @DEG, 1, @GAMMA, @L, @ALPHA, tgkey)
        @BK = @KEY.each_slice(2).map do |kk|
            [
                @TG.sample(kk.first * kk.last) ,        # BK_kk'
                @TG.sample(kk.first * (1 - kk.last)) ,  # BK_k(1-k')
                @TG.sample((1 - kk.first) * kk.last)    # BK_(1-k)k'
            ]
        end

                # ----------------------------------------------------------------------
                #   OLD: Generate bootstrapping keys
                #
                #~ @KPP = Vector.elements(Array.new(@DIM){PolyN.rand(@DEG, Integer, 2)}, false)
                #~ @BK = WTFHE.get_BK(@PREC, @DIM, @KEY, @KPP)
                # ----------------------------------------------------------------------
                #   OLD: Generate key switching keys
                #~ @KS = WTFHE.get_KSK(@TLWE, @PREC, @KEY)   # not extracted yet
    end

    #   Initializes WTFHE cipher with Baby parameters.
    def self.init_baby
        WTFHE.new(*@@BABY_PARAMS)
    end

    #
    #   Implements WTHFE +BlindRotateIm+ algorithm (improved version).
    #
    #   Outputs blindly rotated accumulator (a TRLWE sample) by +k+*+a+ - +b+ = -+mu+.
    #
    #   +ab+::  TLWE/WTFHE sample,
    #   +cf+:: (encrypted and) encoded function values within a TRLWE sample to be rotated (aka. test vector).
    #
    def blind_rotate_im(ab, cf)
        #   check the noise level
        warn "(w) Noise has grown over the safe bound, #{__method__} may corrupt the encrypted data." if ab.to_trlwe_sample.noise.ampl_f >= self.E_pre_bnd
        warn "(w) N.b.! Correct result is not guaranteed for encrypted bootstrapping function." unless cf.to_trlwe_sample.noise.zero?

        #   compute ⌊2N a_i⌉, ⌊2N b⌉
        ab_bar = ab.sample.map do |ai|
            ai.const_term.round_numer_at(@NU + 1)
        end

        #   ACC = X^-b * cf
        acc = PolyN.x_to(-ab_bar[-1], @DEG) * cf

        #   ACC = ((X^aa' - 1)BK_kk' + (X^a - 1)BK_k(1-k') + (X^a' - 1)BK_(1-k)k') ⊡ ACC + ACC
        @BK.each.with_index do |bk_tri, i|
            acc = ((PolyN.x_to(ab_bar[2*i] + ab_bar[2*i+1], @DEG) - 1) * bk_tri[0] + \
                   (PolyN.x_to(ab_bar[2*i],                 @DEG) - 1) * bk_tri[1] + \
                   (PolyN.x_to(ab_bar[2*i+1],               @DEG) - 1) * bk_tri[2]).ext(acc, @GAMMA, @L) + acc
        end

        #   update noise
        acc.update_noise(Noise.br_im(@TAU, @DEG, @DIM, @L, \
                                     self.beta, self.epsilon, \
                                     @BK.first.first.noise, \
                                     cf.noise))

        acc
    end

    #
    #   Implements THFE +SampleExtract+ algorithm in a simplified form for circular bootstrapping keys.
    #
    #   Outputs a TLWE/WTFHE sample.
    #
    #   +acc+:: TRLWE sample (accumulator from +blind_rotate_im+).
    #
    def sample_extract_circ(acc)
        # acc  =  (r_0 + r_1 X + r_2 X^2 +  ...  + r_N-1 X^N-1 | s_0 + s_1 X + ...)
        # result: (r_0, -r_N-1, ..., -r_@DIM, ..., -r_2, -r_1  | s_0)  \
        # under:  (  0,      0, ...,  ___KEY_________________)         / ... with leading zeros omitted           / noise is preserved
        TrlweSample.elements(acc.sample.first.to_a.slice(1..@DIM).reverse.map{|ai|-ai} + [acc.b.const_term], acc.noise)
    end

                # ----------------------------------------------------------------------
                #   OLD: Sample Extract
                #
                #~ def sample_extract(app)
                    #~ ap = []
                    #~ app[0..-2].each do |appi|
                        #~ ap.concat appi.to_polyn.at_x_inverse.to_a
                    #~ end
                    #~ ap << app[-1].const_term
                    #~ TrlweSample.elements(ap, false)
                #~ end

                # ----------------------------------------------------------------------
                #   OLD: Key Switch
                #
                #~ def key_switch(ap)   # inputs a TLWE sample
                    #~ a = TrlweSample.triv_zero(@TLWE.sample_size)
                    #~ a[-1] = ap[-1].clone   # (0, b')
                    #~ @KS.each.with_index do |ksi, i|
                        #~ ksi.each.with_index do |ksij, j|
                            #~ a -= ksij if ap[i].get_bit_at_minus(j+1) == 1   # was: ap[i].const_term...
                        #~ end
                    #~ end
                #~ end

    #
    #   Encrypts +mu+ with TLWE.
    #
    #   +mu+::  plaintext represented as TorusInt with +prec+ precision.
    #
    def encrypt(p)
        warn "(w) Plaintext #{p} out of range, expected in [0,#{(1<<@PREC)-1}]." unless p.is_a?(Integer) and p / (1 << @PREC) == 0
        @TLWE.sample(TorusInt[p.to_i, @PREC])
    end

    #
    #   Decrypts +c+ from TLWE.
    #
    #   +c+::   ciphertext, a TLWE sample.
    #
    def decrypt(c)
        warn "(w) Noise has grown over the safe bound, the value may not be decrypted correctly." if c.to_trlwe_sample.noise.ampl_f >= self.E_max_bnd   # before decryption, the error can be larger than before bootstrapping, where E_pre must be considered

        @TLWE.phase(c).round_at(@PREC).const_term.numer
    end

    #
    #   Encrypts a function for bootstrapping with encrypted function.
    #
    #   +fa+::      function to be encrypted, represented as an Array of 2^(+prec+ - 1) Torus values (n.b. negacyclic property),
    #   +do_encr+:: whether encryption is to be performed (trivial sample is returned otherwise).
    #
    def encr_fn(fa, do_encr = true)
        #   check input array
        fa = fa.to_a.map{|i| TorusInt[i.to_i, @PREC] }
        raise "#{__method__}: Incorrect function representation size, must be equal to 2^(prec-1)!" if fa.size != (1 << (@PREC-1))
        #   extend the array to make it a staircase function
        fa.map!{|e|[e] * ((1 << (@NU - (@PREC - 1))))}.flatten!.rotate!(1 << (@NU - @PREC))
        #   encode it into a polynomial
        fa = PolyN.coeffs(fa, false)

        #   returns a TRLWE sample
        do_encr ? @TG.TRLWE.sample(fa) : @TG.TRLWE.triv_sample(fa)
    end

    #
    #   Bootstrapping algorithm with a function to be evaluated.
    #
    #   For +ab+ (a TLWE/WTFHE sample of +mu+), it outputs a sample of +f+(+mu+), where +f+ is encoded within an array +fa+.
    #
    #   +ab+::  TLWE/WTFHE sample,
    #   +fa+::  function to be evaluated during bootstrapping, represented as an Array of 2^(+prec+ - 1) Torus values (n.b. negacyclic property).
    #
    def bootstrap(ab, fa = nil)
                #~ key_switch(sample_extract(blind_rotate_im(ab, fa)))

        #   in case fa is not given, prepare an identity representation
        fa = (0..((1 << (@PREC-1)) - 1)).to_a if fa.nil?

        #   convert to TorusInt representation & prepare staircase function
        #   (false for a trivial TRLWE sample (0, 0, ..., 0, fa))
        fa = encr_fn(fa, false)

        sample_extract_circ(blind_rotate_im(ab, fa))
    end

    #
    #   Bootstrapping algorithm with an _encrypted_ function to be evaluated.
    #
    #   For +ab+ (a TLWE/WTFHE sample of +mu+), it outputs a sample of +f+(+mu+), where +f+ is encoded and encrypted within +cf+.
    #
    #   +ab+::  TLWE/WTFHE sample,
    #   +cf+::  function to be evaluated during bootstrapping, encrypted as a TRLWE sample.
    #
    def bootstrap_encr_fn(ab, cf)
        sample_extract_circ(blind_rotate_im(ab, cf))
    end


    # ==========================================================================
    #
    #   :section: WTFHE Parameters
    #

    #   Returns plaintext precision.
    def prec
        @PREC
    end

    #   Returns degree of internal TRGSW cipher.
    def deg
        @DEG
    end

    #   Returns WTFHE dimension.
    def dim
        @DIM
    end

    #   Returns noise amplitude +alpha+.
    def alpha
        @ALPHA
    end

    #   Returns decomposition quality beta, see TRGSW.
    def beta
        @TG.beta
    end

    #   Returns decomposition precision epsilon, see TRGSW.
    def epsilon
        @TG.epsilon
    end

    #   Returns WTFHE ciphertext (sample) size.
    def ct_size
        @TLWE.sample_size
    end

    #?# some other parameters?


    # ==========================================================================
    #
    #   :section: Noise Bounds
    #

    #   Returns the bound on rounding error +E_round+ (informative).
    def E_round_bnd
        Noise.round_err_max @DIM, @DEG
    end

    #   Returns the bound on overall maximum error +E_max+.
    def E_max_bnd
        Noise.max_err_max @PREC
    end

    #   Returns the bound on pre-bootstrapping (sample) error +E_pre+.
    def E_pre_bnd
        Noise.pre_err_max @PREC
    end


    # ==========================================================================
    #
    #   :section: Misc methods
    #

    def to_s   # :nodoc:
        "#{self.class.to_s}\n" + \
        "\t[PT prec] #{@PREC}, [Torus prec] #{@TAU}, [deg] #{@DEG}, [dim] #{@DIM},\n" + \
        "\t[gamma] #{@GAMMA}, [l] #{@L}, [alpha] #{@ALPHA},\n" + \
        "\t[key] #{@KEY}"
    end
    alias_method :inspect, :to_s

                # ----------------------------------------------------------------------
                #   OLD: WTFHE initialization
                #
                #~ module InitializationHelper
                    #~ def key_extract(spp, deg)
                        #~ sp = []
                        #~ spp.each do |sppi|
                            #~ sppi = PolyN.const(sppi, deg.to_i) if sppi.is_a? Integer
                            #~ sp.concat sppi.to_polyn.to_a
                        #~ end
                        #~ Vector.elements(sp, false)
                    #~ end

                    #~ def get_KSK(tlwe, prec, spp)
                        #~ sp = key_extract(spp, 1 << (prec-1))
                        #~ ks = []
                        #~ sp.each do |spi|
                            #~ ksi = []
                            #~ prec.times do |j|
                                #~ frac = TorusInt[1 << ($TORUS_INT_PREC - (j+1))]   # frac = 2^-j where j ranges from 1 to prec
                                #~ ksi << tlwe.sample(spi.to_i * frac)
                            #~ end
                            #~ ks << ksi
                        #~ end
                        #~ ks
                    #~ end
                #~ end
                #~ extend InitializationHelper

end
