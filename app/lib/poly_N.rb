require 'torus'
require 'fft'



################################################################################
#
#   +PolyN+ implements polynomials over +Integer+, +Float+, or +Torus+,
#   modulo X^N + 1.
#
class PolyN < Numeric

    class ScalarForPolyN < Numeric   # :nodoc:
        def initialize(value)
            @value = value
        end

        def + polyn
            polyn + @value
        end

        def - polyn
            (-polyn) + @value
        end

        def * polyn
            case @value
            when Numeric
                polyn.map{|e| @value * e}
            else
                raise "Multiplication of #{@value.class} and #{PolyN}|#{polyn.num_type} undefined."
            end
        end

        def / polyn
            raise "Division by #{PolyN} undefined."
        end

        def == polyn
            polyn == @value
        end
    end
    private_constant :ScalarForPolyN   #?# needed?

    module ConversionHelper   # :nodoc:
        private def convert_to_num_array(obj, copy = false)
            case obj
            when Array
                res = copy ? obj.dup : obj
            else
                begin
                    res = obj.to_a
                rescue Exception => e
                    raise TypeError, "can't convert #{obj.class} into an Array (#{e.message})"
                end
                raise TypeError, "#{obj.class}#to_ary should return an Array" unless res.is_a? Array
            end

            case res.first
            when Integer, Float, Complex, Torus
                res.each{|c| raise "#{self.class} initialization error: mixed internal types #{res.first.class} and #{c.class}." unless c.is_a? res.first.class }
            else
                raise "#{self.class} initialization error: invalid internal type #{res.first.class}, #{Integer} or #{Float} or #{Torus} expected."
            end

            res
        end
    end
    extend ConversionHelper

    #   Extend by FFT functions
    include FFT

    #   An array of polynomial coefficients.
    attr_reader :coeffs
    protected :coeffs   #?# what does this do?
    private_class_method :new

    def initialize(coef_ary)
        @coeffs = coef_ary
    end

    #
    #   Initializes a PolyN with coefficients from an array.
    #
    #   +coef_ary+::    an array of coefficients,
    #   +copy+::        flag whether +coef_ary+ or its copy is used internally.
    #
    def self.coeffs(coef_ary, copy = true)
        new convert_to_num_array(coef_ary, copy)
    end

    #
    #   Initializes a PolyN with zero coefficients of given type.
    #
    #   +deg+::         degree N of X^N+1 used for reduction,
    #   +num_type+::    type of coefficients,
    #   +tau+::         Torus precision.
    #
    def self.zero(deg, num_type, tau = nil)
        if num_type <= Integer
            new Array.new(deg, 0)
        elsif num_type <= Float
            new Array.new(deg, 0.0)
        elsif num_type <= Torus
            new Array.new(deg, num_type[0, tau])
        else
            raise "#{self.class} only supports #{Integer} or #{Float} or #{Torus} type, but #{num_type} given."
        end
    end

    #
    #   Initializes a constant PolyN of given degree.
    #
    #   +cterm+::   constant term,
    #   +deg+::     degree N of X^N+1 used for reduction.
    #
    def self.const(cterm, deg = 1)
        case cterm
        when Integer
            new([cterm] + Array.new(deg - 1, 0))
        when Float
            new([cterm] + Array.new(deg - 1, 0.0))
        when Torus
            new([cterm] + Array.new(deg - 1, cterm.zero))
        else
            raise "#{self.class} only supports #{Integer} or #{Float} or #{Torus} type, but #{num_type} given."
        end
    end

    #
    #   Initializes a PolyN with random coefficients of given type (and range).
    #
    #   +deg+::             degree N of X^N+1 used for reduction,
    #   +num_type+::        type of coefficients,
    #   +int_range_tprec+:: Integer range or Torus precision.
    #
    #       PolyN.rand(deg, Integer)            # binary Integer coeffs
    #       PolyN.rand(deg, TorusInt, tau)    # TorusInt coeffs with +tau+ precision
    #
    def self.rand(deg, num_type, int_range_tprec = 2)
        if num_type <= Integer
            new Array.new(deg){Random.rand(int_range_tprec.to_i)}
        elsif num_type <= Float
            new Array.new(deg){Random.rand}
        elsif num_type <= Torus
            new Array.new(deg){num_type[Random.rand(1 << int_range_tprec), int_range_tprec]}
        else
            raise "#{self.class} only supports #{Integer} or #{Float} or #{Torus} type, but #{num_type} given."
        end
    end

    #
    #   Initializes a PolyN with X^power.
    #
    #   +power+::   desired power of X,
    #   +deg+::     degree N of X^N+1 used for reduction.
    #
    def self.x_to(power, deg)
        new([1] + Array.new(deg - 1, 0)).mul_x(power)
    end

    def to_polyn
        self
    end

    #   Returns coeffs as a Vector.
    def to_v
        Vector.elements(@coeffs, true)
    end

    #   Returns coeffs as an Array.
    def to_a
        @coeffs.dup
    end

    #   Returns degree N of X^N+1 used for reduction.
    def deg
        @coeffs.size
    end
    alias_method :n, :deg

    #   Returns constant term.
    def const_term
        @coeffs.first.clone
    end

    #   Returns whether it is a monomial.
    def monomial?
        self.deg == 1
    end

    def map(&block)
        #~ return to_enum(:collect) unless block_given?
        #~ raise "#{self.class}:#{__method__} undefined without a block." unless block_given?   #?#
        self.class.coeffs(@coeffs.map(&block), false)
    end
    alias_method :collect, :map

    def each(&block)
        #~ return to_enum(:each) unless block_given?
        @coeffs.each(&block)
        self
    end

    def each_with_power(&block)
        #~ return to_enum(:each) unless block_given?
        @coeffs.each_with_index(&block)
        self
    end

    def coerce(other)
        [ScalarForPolyN.new(other), self]
    end

    #   Returns L1-norm
    def l1_norm
        case @coeffs.first
        when Integer, Float
            @coeffs.reduce(0){|norm, c| norm + c.abs }
        else
            raise "L1-norm not defined for #{num_type} type."
        end
    end

    #   Returns L2-norm
    def l2_norm
        case @coeffs.first
        when Integer, Float
            Math.sqrt(@coeffs.reduce(0){|norm, c| norm + c**2 })
        else
            raise "L2-norm not defined for #{num_type} type."
        end
    end

    #   Returns numeric type of coeffs.
    def num_type
        #TODO might be mixed of Float and Integer (after some operations)
        @coeffs.first.class
    end

    def == other
        case other
        when PolyN
            @coeffs == other.coeffs
        when Numeric
            raise "Comparison of non-monomial with #{other.class} undefined." unless self.monomial?
            self.const_term == other
        else
            raise "Comparison of #{self.class} and #{other.class} undefined."
        end
    end

    #   Returns coeff at X^power.
    def [](power)
        @coeffs[power]
    end

    def + other
        case other
        when PolyN
            raise "#{self.class} addition error: degree mismatch, #{n} != #{other.n}." unless n == other.n
            self.class.coeffs @coeffs.map.with_index{|ci, i| ci + other[i] }, false
        when Numeric
            self + self.class.const(other, n)
        else
            other.coerce(self).reduce(__method__)
        end
    end

    def -@
        self.class.coeffs @coeffs.map{|c| -c }, false
    end

    def - other
        self + (-other)
    end

    def / other
        self.class.coeffs @coeffs.map{|c| c / other }, false
    end

    #   Returns self multiplied by X^power (mod X^N + 1).
    def mul_x(power = 1)
        power %= 2 * n   #=# shitty syntax highlight
        self.class.coeffs @coeffs.rotate(-power).map.with_index{|e,i| (power - n .. power - 1).include?(i) ? -e : e }, false
    end

    def * other
        case other
        when PolyN   # a special case of Numeric, must be resolved first
            raise "#{self.class} multiplication error: degree mismatch, #{n} != #{other.n}." unless n == other.n

            res = @coeffs.first * other
            (1..n-1).each do |i|
                res += @coeffs[i] * (other.mul_x(i))
            end

            res
        when Numeric
            self.class.coeffs @coeffs.map{|c| c * other }, false
        else
            other.coerce(self).reduce(__method__)   #?# Vector should be now covered by coercion
        end
    end

    #   Returns self at 1/X (mod X^N + 1).
    def at_x_inverse
        # poly_a(1/x) = a_0 - a_(n-1) x - a_(n-2) x^2 - ... - a_2 x^(n-2) - a_1 x^(n-1)
        axi = @coeffs.each_with_index.map do |ak, k|
            if k == 0
                ak
            else
                -@coeffs[n-k]
            end
        end
        self.class.coeffs axi, false
    end

    #   Returns self with Torus coeffs rounded at +newprec+.
    def round_at(newprec)
        self.class.coeffs(coeffs.map{|t| t.round_at(newprec) }, false)
    end

    def to_s   # :nodoc:
        @coeffs.first.to_s + @coeffs[1..-1].each_with_index.map{|e,i| i+=1; (e < 0 ? ' ' : ' + ') + e.to_s + ' X' + (i > 1 ? '^' + i.to_s : '') }.join
    end
    alias_method :inspect, :to_s

end
