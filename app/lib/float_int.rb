
class Integer   # :nodoc:
    def to_fi(bnd, prec)
        FloatInt.from_int(self, bnd, prec)
    end
end

class Float   # :nodoc:
    def to_fi(bnd, prec)
        FloatInt.from_flt(self, bnd, prec)
    end
end

class Torus   # :nodoc:
    def to_fi(bnd, prec)
        raise "Conversion of #{self.class} to #{FloatInt} with different parameters." unless prec == self.prec
        FloatInt.from_torus(self, bnd)
    end
end



################################################################################
#
#   FloatInt implements float using fixed binary precision, stored as a fraction
#   of the form +numerator+/2^+precision+.
#
class FloatInt   #TODO < Numeric?

    class ScalarForFloatInt #TODO < Numeric   # :nodoc:
        def initialize(value)
            @value = value
        end

        def + fi
            fi + @value
        end

        def - fi
            (-fi) + @value
        end

        def * fi
            fi * @value
        end

        def / fi
            raise "Division by #{FloatInt} not implemented yet."
        end

        def == fi
            fi == @value
        end
    end
    private_constant :ScalarForFloatInt   #?# needed?

    #   Numerator of the fraction.
    attr_reader :numer
    #   Bound of the abs value of the fraction (in bits).
    attr_reader :BND
    #   Precision of the fraction, i.e., denominator = 2^+PREC+.
    attr_reader :PREC

    private def initialize(numer, bnd = nil, prec = nil)
        return numer.clone if numer.is_a? FloatInt

        @BND = bnd.to_i
        raise "#{self.class} initialization error: bound cannot be <= 0." if @BND <= 0

        if numer.is_a? TorusInt
            @PREC = numer.prec
        else
            @PREC = prec.to_i
        end

        raise "#{self.class} initialization error: precision cannot be <= 0." if @PREC <= 0

        case numer
        when TorusInt
            raise "The value #{numer.numer} is out of bounds (more than #{@BND + @PREC} bits)." if numer.numer >= (1 << (@BND + @PREC))
            @numer = numer.numer
        when Integer
            raise "The value #{numer} is out of bounds (more than #{@BND + @PREC} bits)." if numer.abs >= (1 << (@BND + @PREC))
            @numer = numer
        when Float
            raise "The value #{numer} is out of bounds (more than #{@BND + @PREC} bits)." if (numer * (1 << @PREC)).round.abs >= (1 << (@BND + @PREC))
            @numer = (numer * (1 << @PREC)).round
        else
            raise "#{self.class} initialization error: #{numer.class} class is not supported."
        end
    end

    #
    #   Initializes a FloatInt with a numerator, a precision and a bound.
    #
    #--
    #   +numer+:: // fraction numerator,
    #   +bnd+::   // fraction bit-precision, i.e., denominator = 2^+prec+.
    #   +prec+::  // fraction bit-precision, i.e., denominator = 2^+prec+.
    #
    def self.[](numer, bnd, prec)
        FloatInt.new(numer, bnd, prec)
    end

    #
    #   Initializes a FloatInt from a Torus.
    #
    #--
    #   +bnd+::   // fraction bit-precision, i.e., denominator = 2^+prec+.
    #
    def self.from_torus(torus, bnd)
        case torus
        when TorusInt
            FloatInt.new(torus.numer, bnd, torus.prec)
        else
            raise "#{__method__} not supported for other type than #{TorusInt}."
        end
    end

    #   Initializes a FloatInt equal to zero with bound +bnd+
    #   and precision +prec+.
    def self.zero(bnd, prec)
        FloatInt.new(0, bnd, prec)
    end

    #   Returns a new FloatInt equal to zero with the same precision and bound.
    def zero
        self.class.zero(@BND, @PREC)
    end

    #   Returns TODO.
    def self.from_int(int, bnd, prec)
        FloatInt.new(int.to_i << prec.to_i, bnd, prec)
    end

    #   Returns TODO.
    def from_int(int)
        self.class.from_int(int, @BND, @PREC)
    end

    #   Returns TODO.
    def self.from_flt(flt, bnd, prec)
        FloatInt.new(flt.to_f, bnd, prec)
    end

    #   Returns TODO.
    def from_flt(flt)
        self.class.from_flt(flt, @BND, @PREC)
    end

    def clone
        self.class[@numer, @BND, @PREC]
    end

    #   Returns self and fails when called on an incompatible object.
    def to_fi
        self
    end

    #   Bound of its absolute value.
    def bnd
        @BND
    end

    #   Precision of the fractional part, i.e., denominator = 2^+PREC+.
    def prec
        @PREC
    end

    def to_s(base = 2)
        case base
        when 2
            sprintf("%s%0*b", @numer < 0 ? "-" : "+", @BND, (@numer.abs >> @PREC) & ((1 << @BND) - 1)) + "." + sprintf("%0*b", @PREC, @numer.abs & ((1 << @PREC) - 1))
        when 10
            self.to_f.to_s
        else
            @numer.to_s(base) + "_#{base} / 2^#{@PREC}"
        end
    end
    alias_method :inspect, :to_s

    def to_f
        @numer.to_f / (1 << @PREC)
    end

    def * other
        case other
        when Integer
            self.class.new(@numer * other, @BND, @PREC)
        when Float, Torus
            [self, self.class.new(other, @BND, @PREC)].reduce(__method__)
        when FloatInt
            raise "#{self.class}##{__method__} error: incompatible numbers with #{@BND} and #{other.bnd} bits of bound, and #{@PREC} and #{other.prec} bits of precision, respectively." unless @BND == other.bnd and @PREC == other.prec
            n2 = (@numer * other.numer) >> (@PREC - 1)
            self.class.new((n2 >> 1) + (n2 & 1), @BND, @PREC)
        else
            other.coerce(self).reduce(__method__)   #TODO
        end
    end

    def + other
        case other
        when Integer
            [self, self.from_int(other)].reduce(__method__)
        when Float, Torus
            [self, self.class.new(other, @BND, @PREC)].reduce(__method__)
        when FloatInt
            raise "#{self.class}##{__method__} error: incompatible numbers with #{@BND} and #{other.bnd} bits of bound, and #{@PREC} and #{other.prec} bits of precision, respectively." unless @BND == other.bnd and @PREC == other.prec
            self.class.new(@numer + other.numer, @BND, @PREC)
        else
            other.coerce(self).reduce(__method__)   #TODO
        end
    end

    def -@
        self.class.new(-@numer, @BND, @PREC)
    end

    def - other
        self + (-other)
    end

    def == other
        case other
        when Integer
            [self, self.from_int(other)].reduce(__method__)
        when Float, Torus
            [self, self.class.new(other, @BND, @PREC)].reduce(__method__)
        when FloatInt
            self.numer << other.prec == other.numer << self.prec
        when Numeric
            other.coerce(self).reduce(__method__)   #TODO
        else
            raise "Comparison of #{self.class} and #{other.class} undefined." unless other.is_a? self.class
        end
    end

    def coerce(other)
        [ScalarForFloatInt.new(other), self]
    end

end

__END__

    #   Returns a numerator rounded at precision +newprec+, i.e., +round+(+frac+ * 2^+newprec+).
    def round_numer_at(newprec)
        (@numer >> (@PREC - newprec)) + ((@numer >> (@PREC - newprec - 1)) & 1)
    end

    #   Returns a TorusInt rounded at precision +newprec+.
    def round_at(newprec)
        self.class[round_numer_at(newprec), newprec]
    end

    #   Returns a bit of numerator rounded at +newprec+ (cf. #round_numer_at) at position +j+.
    def get_bit_at_minus(j, newprec = @PREC)   # from 2^-1 till 2^-prec
        j = j.to_i
        raise "#{self.class}:#{__method__} invalid argument." if j <= 0 or j > @PREC or j > newprec
        (round_numer_at(newprec) >> (newprec - j)) & 1
    end

    #   Returns the numerator (rounded and) decomposed into a Vector of +l+ +bg_bits+-bit numbers.
    #   E.g., 11011 10011 101 -> (11011, 10111) for +bg_bits+ = 5 and +l+ = 2.
    def decompose(bg_bits, l)
        r = round_numer_at(bg_bits * l)
        d = Vector.zero(l)

        l.times do |p|
            d[l-1-p] = (r >> (bg_bits * p)) & ((1 << bg_bits) - 1)   # shift to the right and extract +bg_bits+
        end

        d
    end

end
