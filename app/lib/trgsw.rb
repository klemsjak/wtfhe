require 'matrix'
require 'torus_int'
require 'poly_N'
require 'trlwe'
require 'trlwe_sample'
require 'trgsw_sample'



################################################################################
#
#   Implementation of TRGSW cipher.
#
class TRGSW

    #   Underlying TRLWE cipher.
    attr_reader :TRLWE

    #
    #   Initializes TRGSW cipher.
    #
    #   +tau+::       underlying Torus precision,
    #   +dim+::         dimension,
    #   +key+::         private key, an Array/Vector of binary polynomials modulo X^N + 1,
    #
    def initialize(tau, deg, dim, gamma, l, alpha = 0, key = nil)
        # initialize underlying TRLWE
        @TRLWE = TRLWE.new(tau, deg, dim, alpha, key)

        @GAMMA = gamma.to_i
        @L = l.to_i

        # check gadget matrix resolution
        warn "(w) #{self} initialization: decomposition level is unnecessarily large." if @GAMMA * @L > tau.to_i
    end

    #   Sample size: rows.
    def sample_rows
        @TRLWE.sample_size * @L
    end

    #   Sample size: columns.
    def sample_cols
        @TRLWE.sample_size
    end

    #   Sample size: [rows, columns].
    def sample_dims
        [sample_rows, sample_cols]
    end

    #   Returns decomposition quality beta = B_g / 2; cf. [CGGI20], Lemma 3.7.
    def self.beta(gamma)
        # B_g = 1 << gamma
        (1 << (gamma-1))
    end

    def beta   # :nodoc:
        self.class.beta(@GAMMA)
    end

    #   Returns decomposition precision epsilon = 1 / 2 B_g^l; cf. [CGGI20], Lemma 3.7.
    def self.epsilon(gamma, l)
        1.0 / (1 << 1 + gamma * l)
    end

    def epsilon   # :nodoc:
        self.class.epsilon(@GAMMA, @L)
    end

    #
    #   Returns the Gadget Matrix aka +H+ (multiplied by an Integer /polynomial/ +mu+).
    #
    #   +mu+::  factor (PolyN|Integer or Integer) to multiply the gadget matrix +H+.
    #
    def gadget_times(mu = 1)
        # compute 1/2^(gamma*l)
        t = TorusInt[1, @GAMMA * @L]
        t = mu * t

        a = Array.new(@L, nil)
        # in each iteration, multiply by 2^gamma (equals B_g)
        a.each.with_index do |e,i|
            a[-i-1] = t
            t = (1 << @GAMMA) * t
        end

        mu_h = Matrix.zero(sample_rows, sample_cols)

        sample_cols.times do |i|
            @L.times do |j|
                mu_h[i * @L + j, i] = a[j]
            end
        end

        mu_h
    end

    #
    #   Returns a fresh TRGSW sample of +mu+.
    #
    #   +mu+::      message (PolyN over Integer) to be sampled.
    #--
    #TODO: check out what range can be encrypted
    #++
    #
    def sample(mu)
        z = Matrix.zero(0, @TRLWE.sample_size)

        sample_rows.times do |i|
            #TODO: make this more effective?
            z = z.vstack(Matrix.row_vector @TRLWE.homog_sample.sample)
        end

        TrgswSample.elements(z + gadget_times(mu), @TRLWE.noise_inst)
    end

    def to_s   # :nodoc:
        "#{self.class.to_s}\n" + \
        "\t[#{@TRLWE.class}] #{@TRLWE},\n" + \
        "\t[gamma] #{@GAMMA}, [l] #{@L}"
    end
    alias_method :inspect, :to_s

end
