require 'matrix'
require 'sample'



################################################################################
#
#   Implementation of TRGSW sample.
#
class TrgswSample < Sample

    def initialize(matrix, noise)   # :nodoc:
        super
    end

    #TODO: consider copy param
    def TrgswSample.elements(matrix, noise)
        raise "#{TrgswSample} initialization: row count must be a multiple of column count." unless matrix.row_count % matrix.column_count == 0
        TrgswSample.new(matrix.to_matrix, noise)
    end

    def to_trgsw_sample
        self
    end

    #   Sample size: rows.
    def rows
        @sample.row_count
    end

    #   Sample size: columns.
    def cols
        @sample.column_count
    end

    #   Sample size: [rows, columns].
    def dims
        [rows, cols]
    end

    #   Sample precision: rows / columns.
    def prec
        rows / cols
    end

    # Implements TFHE External Product: TRGSW x TRLWE -> TRLWE.
    def ext(b, gamma, l)
        #             A ⊡ b = Dec(b)^T                              · A
        TrlweSample.elements((b.to_trlwe_sample.decompose(gamma, l) * @sample)[0,0..-1], \
                             @noise.ext(b.noise, 1, l, b.deg, TRGSW.beta(gamma), TRGSW.epsilon(gamma, l)))   #TODO: pass beta and epsilon some better way
    end
end
