require 'torus'



################################################################################
#
#   This class implements noise operations for TRLWE/TRGSW samples.
#
class Noise

    attr_reader :TAU

    private_class_method :new

    def initialize(tau, ampl_i, var)
        @TAU    = tau
        @ampl_i = ampl_i
        @var    = var
    end

    #
    #   Creates an instance of +Noise+ by all parameters.
    #
    #   +tau+::     +TorusInt+ precision
    #   +ampl_i+::  Noise amplitude (+Integer+ multiples of 1/2^+tau+)
    #   +var+::     Noise variance (+Float+)
    #
    def self.by_params(tau, ampl_i, var)
        new tau.to_i, ampl_i.to_i, var.to_f
    end

    #   Returns new instance of +Noise+ by amplitude (+Integer+ multiples of 1/2^+tau+) and variance (+Float+).
    def new_by_ampl_i_var(ampl_i, var)
        self.class.by_params @TAU, ampl_i, var
    end

    #   Creates an instance of +Noise+ (expects uniform distribution) by +TorusInt+ precision +tau+ and amplitude (+Integer+ multiples of 1/2^+tau+).
    def self.by_ampl_i_uniform(tau, ampl_i)
        new \
            tau, \
            ampl_i, \
            (ampl_i ** 2).to_f / (3 * (1 << (2 * tau)))   # variance of uniform distribution on [-ampl, ampl]
    end

    #   Creates an instance of zero +Noise+ by +TorusInt+ precision +tau+.
    def self.zero(tau)
        new tau, 0, 0.0
    end

    #   Returns new instance of +Noise+ from itself.
    def clone
        self.class.by_params @TAU, @ampl_i, @var
    end

    #   Returns new instance of zero +Noise+ from itself.
    def zero
        new_by_ampl_i_var 0, 0.0
    end

    #   Returns whether noise is zero.
    def zero?
        @ampl_i == 0
    end

    #   Returns amplitude as a float.
    def ampl_f
        @ampl_i.to_f / (1 << @TAU)
    end

    #   Returns amplitude as a numerator of the form <+ampl_i+> / 2^+tau+.
    def ampl_i
        @ampl_i
    end

    #   Returns variance.
    def var
        @var
    end

    #   Returns standard deviation.
    def stddev
        Math.sqrt(@var)
    end

    #   Draws & returns a fresh sample of noise, type +TorusInt+.
    def draw_torus(alpha)
        #TODO: add Gaussian distro? custom RNG?
        TorusInt[rand(2 * alpha.to_i + 1) - alpha.to_i, @TAU]
    end

    #   Draws & returns a fresh sample of noise, type +PolyN+ over +TorusInt+.
    def draw_poly(alpha, deg = 1)
        PolyN.coeffs(Array.new(deg.to_i){draw_torus(alpha)})
    end

    def to_noise   # :nodoc:
        self
    end

    #
    #   Returns the bound on rounding error +E_round+ (informative).
    #
    #   +dim+::     WTFHE dimension
    #   +deg+::     WTFHE degree
    #
    def self.round_err_max(dim, deg)
        (dim.to_i + 1.0) / (4 * deg.to_i)
    end

    #
    #   Returns the bound on overall maximum error +E_max+.
    #
    #   +prec+::    WTFHE plaintext precision.
    #
    def self.max_err_max(prec)
        1.0 / (1 << (prec.to_i + 1))
    end

    #
    #   Returns the bound on pre-bootstrapping (sample) error +E_pre+.
    #
    #   +prec+::    WTFHE plaintext precision.
    #
    def self.pre_err_max(prec)
        Noise.max_err_max(prec) / 2   # half of E_max
    end


    # ==========================================================================
    #
    #   :section: Methods for noise combination
    #

    #   Returns resulting noise after ciphertext addition
    def add(other_noise)
        raise "Different precisions of noise in #{__method__}!" if @TAU != other_noise.to_noise.TAU

        # amplitudes & vars sum up together
        #!#TODO what about their dependency? for Var, this only holds for independent samples ...
        new_by_ampl_i_var \
            @ampl_i + other_noise.ampl_i, \
            @var + other_noise.var
    end
    alias_method :sum, :add

    #   Returns resulting noise after multiplication by +scalar+ (+Integer+, +Float+, or +PolyN+).
    def scalar_mult(scalar)
        l1s = scalar.is_a?(PolyN) ? scalar.l1_norm : scalar.abs
        l2s = scalar.is_a?(PolyN) ? scalar.l2_norm : scalar.abs

        # amplitude & var gets multiplied by l1-norm & l2-norm squared, respectively
        new_by_ampl_i_var \
            l1s    * @ampl_i, \
            l2s**2 * @var
    end

    #
    #   Returns resulting noise after BlindRotateIm.
    #
    #   +deg+::     degree, aka. +N+,
    #   +dim+::     dimension of WTFHE, aka. +n+,
    #   +l+::       decomposition depth,
    #   +beta+::    decomposition quality; +beta+ = +B_g+ / 2,
    #   +eps+::     decomposition precision; +eps+ = 1 / 2 +B_g+^+l+,
    #   +e_bk+::    error of bootstrapping keys,
    #   +var_bk+::  variance of bootstrapping keys,
    #   +e_cf+::    error of (encrypted) test vector,
    #   +var_cf+::  variance of (encrypted) test vector.
    #
    def self.br_im(tau, deg, dim, l, beta, eps, bk_noise, cf_noise)
        # amplitude & variance
        Noise.by_params \
            tau, \
            6*dim*l*deg*beta    * bk_noise.ampl_i + dim*(1+deg)*eps    + cf_noise.ampl_i, \
            6*dim*l*deg*beta**2 * bk_noise.var    + dim*(1+deg)*eps**2 + cf_noise.var
    end

    #
    #   Returns resulting noise after external product.
    #
    #   +k+::       dimension of TRLWE (+k+ = 1),
    #   +l+::       decomposition depth,
    #   +deg+::     degree, aka. +N+,
    #   +beta+::    decomposition quality; +beta+ = +B_g+ / 2,
    #   +eps+::     decomposition precision; +eps+ = 1 / 2 +B_g+^+l+,
    #   +mu_g_l1+:: L1-norm of A's plaintext,
    #   +mu_g_l2+:: L2-norm of A's plaintext.
    #
    def ext(b_noise, k, l, deg, beta, eps, mu_g_l1 = 1, mu_g_l2 = 1)
        raise "Different precisions of noise in #{__method__}!" if @TAU != b_noise.to_noise.TAU

        # amplitude & variance
        # cf. [CGGI20], Thm. 3.13 & Cor. 3.14, respectively
        new_by_ampl_i_var \
            (k+1)*l*deg*beta    * @ampl_i + mu_g_l1  * (1+k*deg)*eps    + mu_g_l1    * b_noise.ampl_i, \
            (k+1)*l*deg*beta**2 *    @var + mu_g_l2**2*(1+k*deg)*eps**2 + mu_g_l2**2 * b_noise.var
    end


    # ==========================================================================
    #
    #   :section: Misc methods
    #

    def to_s   # :nodoc:
        "#{self.class                }: [ampl]   #{@ampl_i}/2^#{@TAU} (#{self.ampl_f}),\n" + \
        ' ' * self.class.to_s.size + "  [var]    #{@var},\n" + \
        ' ' * self.class.to_s.size + "  [stddev] #{self.stddev}"
    end
    alias_method :inspect, :to_s

end
