################################################################################
#
#   DEMO: Polynomials, Vectors of Polynomials
#
zz1 = PolyN.rand(3, Integer, 10)
zz2 = PolyN.rand(3, Integer, 10)
zz3 = PolyN.rand(3, Integer, 10)
zzv = Vector[zz1, zz2, zz3]

tt1 = PolyN.rand(3, TorusInt)
tt2 = PolyN.rand(3, TorusInt)
tt3 = PolyN.rand(3, TorusInt)
ttv = Vector[tt1, tt2, tt3]

zz1 * tt1
zz1 * ttv
zzv.dot ttv
