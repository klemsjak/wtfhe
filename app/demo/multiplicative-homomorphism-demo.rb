################################################################################
#
#   DEMO: Homomorphic Multiplication
#

# ==============================================================================
#   Setup
#

# ------------------------------------------------------------------------------
#   Parameters
#
prec = 2        # plaintext precision pi
tau = prec      # torus (sample) precision tau (equal to pi; note that there is no encryption, only external product)
k = 4           # TRGSW dimension
deg = 8         # TRGSW degree
gamma = 2       # decomposition parameter gamma
l = 1           # decomposition parameter l
alpha = 0       # noise amplitude

key = Vector.elements(Array.new(k){PolyN.rand(deg, Integer, 2)}, false)

# ------------------------------------------------------------------------------
#   Ciphers
#
tg = TRGSW.new(tau, deg, k, gamma, l, alpha, key)
tl = tg.TRLWE

# ------------------------------------------------------------------------------
#   Plaintexts
#
m_i = PolyN.rand(deg, Integer, 1 << prec)     # Integer polynomial for TRGSW
puts "m_i = #{m_i}"
m_p = PolyN.rand(deg, TorusInt, tau)          # Torus polynomial for TRLWE
puts "m_p = #{m_p}"


# ==============================================================================
#   Homomorphic Multiplication
#

# ------------------------------------------------------------------------------
#   Encrypt
#
aa = tg.sample m_i
puts "A = TRGSW(m_i) = #{aa}"
b = tl.sample m_p
puts "b = TRLWE(m_p) = #{b}"

# ------------------------------------------------------------------------------
#   Plain and Homomorphic Multiplication (External Product A ⊡ b)
#
plain_mult = m_i * m_p
puts "Plain multiplication:"
puts "m_i * m_p = #{plain_mult}"
homo_mult = aa.ext(b, gamma, l)
puts "Homomorphic multiplication:"
puts "A ⊡ b = #{homo_mult}"


# ==============================================================================
#   Check Homomorphism
#
puts "m_i * m_p == TRLWE^-1(A ⊡ b)"
plain_mult == tl.phase(homo_mult)
