################################################################################
#
#   DEMO: Homomorphic Addition
#

# ==============================================================================
#   Setup
#

# ------------------------------------------------------------------------------
#   Cipher
#
w = WTFHE.init_baby

# ------------------------------------------------------------------------------
#   Plaintexts & Constants
#
p1 = 1
p2 = 2

# to get closer to E_pre bound (1/2^4), need to add ord. 2^16 values (less)
e1 = 2**15 + 1
e2 = 2**14 + 2


# ==============================================================================
#   Homomorphic Addition
#

# ------------------------------------------------------------------------------
#   Encrypt
#
c1 = w.encrypt p1
puts "c1 = WTFHE(p1) = #{c1}"
c2 = w.encrypt p2
puts "c2 = WTFHE(p2) = #{c2}"

# ------------------------------------------------------------------------------
#   Plain and Homomorphic Multiplication (External Product A ⊡ b)
#
plain_sum = e1*p1 + e2*p2
puts "Plain addition:"
puts "e1*p1 + e2*p2 = #{plain_sum}"
homo_sum = e1*c1+e2*c2
puts "Homomorphic addition:"
puts "e1*c1 + e2*c2 = #{homo_sum}"


# ==============================================================================
#   Bootstrapping
#
c1b = w.bootstrap c1
c2b = w.bootstrap c2
homo_sum_b = w.bootstrap homo_sum
puts "Bootstrapping:"
puts "c1 = #{c1}"
puts "bootstrap(c1) = #{c1b}"
puts "e1*c1 + e2*c2 = #{homo_sum}"
puts "bootstrap(e1*c1 + e2*c2) = #{homo_sum_b}"


# ==============================================================================
#   Check Homomorphism
#
puts "e1*p1 + e2*p2 == WTFHE^-1(e1*c1 + e2*c2)"
#   plaintexts operate modulo 2^prec
plain_sum % (1 << w.prec) == w.decrypt(homo_sum)

puts "WTFHE^-1(bootstrap(e1*c1 + e2*c2)) = #{w.decrypt(homo_sum_b)}"
