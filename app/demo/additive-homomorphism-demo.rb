################################################################################
#
#   DEMO: Homomorphic Addition
#

# ==============================================================================
#   Setup
#

# ------------------------------------------------------------------------------
#   Parameters
#
prec = 2        # plaintext precision pi
tau = 2         # torus (sample) precision tau
k = 4           # TRGSW dimension
deg = 8         # TRGSW degree
gamma = 2       # decomposition parameter gamma
l = 1           # decomposition parameter l
alpha = 0       # noise amplitude

key = Vector.elements(Array.new(k){PolyN.rand(deg, Integer, 2)}, false)

# ------------------------------------------------------------------------------
#   Cipher
#
tl = TRLWE.new(tau, deg, k, alpha, key)

# ------------------------------------------------------------------------------
#   Plaintexts & Constants
#
mu1 = PolyN.rand(deg, TorusInt, tau)
mu2 = PolyN.rand(deg, TorusInt, tau)

e1 = PolyN.rand(deg, Integer, 10)       # 10 = integer range
e2 = PolyN.rand(deg, Integer, 10)


# ==============================================================================
#   Homomorphic Addition
#

# ------------------------------------------------------------------------------
#   Encrypt
#
ct1 = tl.sample mu1
puts "c1 = TRLWE(mu1) = #{ct1}"
ct2 = tl.sample mu2
puts "c2 = TRLWE(mu1) = #{ct2}"

# ------------------------------------------------------------------------------
#   Plain and Homomorphic Multiplication (External Product A ⊡ b)
#
plain_sum = e1*mu1 + e2*mu2
puts "Plain addition:"
puts "e1*mu1 + e2*mu2 = #{plain_sum}"
homo_sum = e1*ct1+e2*ct2
puts "Homomorphic addition:"
puts "e1*ct1 + e2*ct2 = #{homo_sum}"


# ==============================================================================
#   Check Homomorphism
#
puts "e1*mu1 + e2*mu2 == TRLWE^-1(e1*ct1 + e2*ct2)"
plain_sum == tl.phase(homo_sum)
