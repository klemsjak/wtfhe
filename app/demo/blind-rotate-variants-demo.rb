################################################################################
#
#   DEMO: Variants of BlindRotate
#

# ==============================================================================
#   Setup
#

# ------------------------------------------------------------------------------
#   Parameters
#
prec = 2        # plaintext precision pi
tau = 20        # torus (sample) precision tau
dim = 4         # TLWE dimension n
k = 1           # TRGSW dimension k = 1 (fixed)
gamma = 3       # decomposition parameter gamma
l = 4           # decomposition parameter l
alpha = 1       # noise amplitude

#~ # calculate sufficient degree
#~ nu = Math.log2(dim + 1).ceil + prec   # N = 2^nu where nu ~ log2(n) + prec (i.e., N ~ (n+1) * 2^prec)
#~ deg = 1 << nu

# hack with key with limited Hamming weight
deg = 16        # now we need to use TLWE key with Hamming weight <= 3
begin
    key = Vector.elements(Array.new(k){PolyN.rand(deg, Integer, 2)}, false)
end while (key.first.l1_norm > 3)

# ------------------------------------------------------------------------------
#   Ciphers
#
tg = TRGSW.new(tau, deg, k, gamma, l, alpha, key)
tr = tg.TRLWE

# ------------------------------------------------------------------------------
#   Plaintexts: Integer and Torus Polynomial
#
p_int = 3
p_tpoly = PolyN.coeffs((1..deg).to_a.map{|p|TorusInt[p,prec]})


# ==============================================================================
#   Sample plaintexts
#
aa = tg.sample(p_int)
puts "A = #{aa}"
b = tr.sample(p_tpoly)   # tr.sample(PolyN.rand(deg, TorusInt, tau))
puts "b = #{b}"


# ==============================================================================
#   Calculate one step of BlindRotate two different ways
#

# ------------------------------------------------------------------------------
#   Prepare rotation polynomial
#
a = 3
xa = PolyN.x_to(a,deg)

# ------------------------------------------------------------------------------
#   Calculate both intermediates (note different noise amplitude!)
#
resa = ((xa - 1) * aa).ext( b , gamma, l) + b
puts "(X^a - 1) A ⊡ b + b = #{resa}"

resb = aa.ext( xa * b - b , gamma, l) + b
puts "A ⊡ ((X^a - 1)·b) + b = #{resb}"


# ==============================================================================
#   Check that they encrypt the same
#
tr.phase(resa).round_at(prec) == tr.phase(resb).round_at(prec)
