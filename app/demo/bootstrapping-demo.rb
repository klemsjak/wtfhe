################################################################################
#
#   DEMO: WTFHE Bootstrapping
#

# ==============================================================================
#   Setup
#

# ------------------------------------------------------------------------------
#   Cipher
#
w = WTFHE.init_baby


# ==============================================================================
#   Run Bootstrapping with custom function encoded in Test Vector (tv)
#
tv = [ 1, 2 ]
#   Note negacyclic extension
(1 << w.prec).times do |i|
    puts "WTFHE^-1(eval_f(WTFHE(#{i}))) =  #{w.decrypt(w.bootstrap(w.encrypt(i), tv))}"
end

# ==============================================================================
#   Run Bootstrapping with encrypted function
#
ctv = w.encr_fn(tv)
#   Note negacyclic extension
(1 << w.prec).times do |i|
    puts "WTFHE^-1(eval_encrf(WTFHE(#{i}))) =  #{w.decrypt(w.bootstrap_encr_fn(w.encrypt(i), ctv))}"
end
