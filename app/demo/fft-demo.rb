################################################################################
#
#   DEMO: Fast Fourier Transform & Variants
#

# ==============================================================================
#   Setup
#

# ------------------------------------------------------------------------------
#   Polynomials
#
pc = [1,2,-3,-1]
qc = [-2,0,-1,3]

p = PolyN.coeffs(pc)
q = PolyN.coeffs(qc)

# ------------------------------------------------------------------------------
#   Negacyclic extension
#
pnc = pc + pc.map(&:-@)
qnc = qc + qc.map(&:-@)

pn = PolyN.coeffs(pnc)
qn = PolyN.coeffs(qnc)

# ------------------------------------------------------------------------------
#   Zero extension
#
pzc = pc + Array.new(pc.size,0)
qzc = qc + Array.new(qc.size,0)

pz = PolyN.coeffs(pzc)
qz = PolyN.coeffs(qzc)

# ------------------------------------------------------------------------------
#   Plain polynomial multiplication
#
r = (p * q)
rc = r.send :coeffs


# ==============================================================================
#   Plain FFT of zero extension
#
fpz = pz.fft_plain
fqz = qz.fft_plain
frz = fpz.point_wise fqz
rc
frz.ifft_plain


# ==============================================================================
#   Plain FFT of negacyclic extension
#
fpn = pn.fft_plain
fqn = qn.fft_plain
frn = fpn.point_wise fqn
rc
frn.ifft_plain


# ==============================================================================
#   Negacyclic FFT
#
fnp = p.fnt
fnq = q.fnt
fnr = fnp.point_wise fnq
rc
fnr.ifnt


# ==============================================================================
#   Accelerated Negacyclic FFT
#
afnp = p.afnt
afnq = q.afnt
afnr = afnp.point_wise afnq
rc
afnr.iafnt


# ==============================================================================
#   Relations between variants
#

# ------------------------------------------------------------------------------
#   in terms of classical FFT:
p.fft_plain
#   plain inverse
FourierImage[(-1.0+0.0i), (4.0-3.0i), (-3.0+0.0i), (4.0+3.0i)].ifft_plain

# ------------------------------------------------------------------------------
#   in terms of AFNT:
p.afnt
#   AFNT inverse
FourierImage[(3.121+2.293i), (-1.121-3.707i)].iafnt

#   inverse AFNT equals FNT of Hermitian extension
FourierImage[ (3.121+2.293i), (-1.121-3.707i), (-1.121+3.707i), (3.121-2.293i) ].ifnt

#   inverse AFNT also equals plain FFT of Hermitian extension multiplied by the second column of omega_N
FourierImage[(3.121+2.293i), (-1.121-3.707i), -1.121+3.707i, 3.121-2.293i].ifft_plain.map.with_index{|e,i| e * (Math::cos(Math::PI * i / 4) + 1i * Math::sin(Math::PI * i / 4)) }.map{|e| e.round(3) }
