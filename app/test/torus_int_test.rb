#!/usr/bin/env -S ruby -Itest

require 'test/unit'
$LOAD_PATH.unshift(File.dirname(__FILE__) + "/../lib/")

# ---   load tested class   ---
require 'torus_int'

# ---   define test class(es)   ---
class TorusIntTest < Test::Unit::TestCase
    @@t = TorusInt[7, 6]
    @@u = TorusInt[38, 6]

    def test_clone
        assert_equal @@t, @@t.clone
    end

    def test_prec
        assert_equal 6, @@t.prec
    end

    def test_to_s
        assert_equal '7/2^6', @@t.to_s
    end

    def test_to_f
        assert_in_delta 0.109375, @@t.to_f, 0.000001
    end

    def test_scalar_mult
        assert_equal TorusInt[55, 6], @@t.scalar_mult(17)
    end

    def test_plus
        assert_equal TorusInt[4, 6], @@t + TorusInt[61, 6]
        assert_equal TorusInt[11, 8], @@t + TorusInt[239, 8]
        assert_raise RuntimeError do
            @@t + 5
        end
        assert_raise RuntimeError do
            @@t + 0.15
        end
    end

    #TODO plus something to be coerced, e.g., PolyN over TorusInt

    def test_minus_self
        assert_equal TorusInt[57, 6], -@@t
    end

    def test_equal
        assert_equal true, @@t == TorusInt[28, 8]
    end

    def test_round_numer_at
        assert_equal 4, @@t.round_numer_at(5)
    end

    def test_round_at
        assert_equal TorusInt[4, 5], @@t.round_at(5)
    end

    def test_get_bit_at_minus
        assert_equal 0, @@u.get_bit_at_minus(3)
        assert_equal 1, @@u.get_bit_at_minus(3,3)
        assert_equal 1, @@u.get_bit_at_minus(4)
        assert_raise RuntimeError do
            @@u.get_bit_at_minus(4,3)
        end
    end

    def test_decompose
        assert_equal Vector[6, 4], TorusInt[206,8].decompose(3,2)
    end
end
