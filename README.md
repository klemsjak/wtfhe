
# WTFHE

*neural-netWork-ready Torus Fully Homomorphic Encryption*


## Description

WTFHE is a Ruby implementation of TFHE, which is only intended for rapid prototyping of novel modifications or enhancements of TFHE.


## Usage

Run

    app/bin$ ./wtfhe

and use the methods described within the WTFHE class, which is fully loaded. Enjoy!

## Examples

Find example code in `app/demo`. Can be directly put into running WTFHE console as described above.
